

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
};



// fetch method
//syntax:  ('url', options)

fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data));

//add post
document.querySelector('#form-add-post').addEventListener("submit",(e) => {
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			body: JSON.stringify({
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 1
			}),
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			alert('Post successfully added');

			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;
		});

});

//edit post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
};

//update post

document.querySelector('#form-edit-post').addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}

	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert("Post Updated");

	//mini activity
	document.querySelector('#txt-edit-title').value = null;
	document.querySelector('#txt-edit-body').value = null;
	document.getElementById("btn-submit-update").disabled = true;

	})


});

//activity
// fetch('https://jsonplaceholder.typicode.com/posts/1', {
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
	    method: 'DELETE',
	    headers: {
	      'Content-Type': 'application/json'
	    },
	    body: JSON.stringify()
	})
	.then(response => {
	    return response.json( )
	})
	.then(data => 
	    // this is the data we get after doing the delete request, do whatever you want with this data
	    console.log(data) 
	);

	//remove post from view only
document.querySelector(`#post-${id}`).remove();
}



















